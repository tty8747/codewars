# In this little assignment you are given a string of space separated numbers, and have to return the highest and lowest number.

## Example:

```go
highAndLow("1 2 3 4 5");  // return "5 1"
highAndLow("1 2 -3 4 5"); // return "5 -3"
highAndLow("1 9 3 4 -5"); // return "9 -5"
```

## Notes:

- All numbers are valid Int32, no need to validate them.
- There will always be at least one number in the input string.
- Output string must be two numbers separated by a single space, and highest number is first.

## Solution:

```go
package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	fmt.Println(HighAndLow("1 2 3 4 5"))
	fmt.Println(HighAndLow("1 2 -3 4 5"))
	fmt.Println(HighAndLow("1 9 3 4 -5"))
}

func HighAndLow(in string) string {
	a := strings.Split(in, " ")
	b := make([]int, 0, 0)

	for _, elem := range a {
		c, err := strconv.Atoi(elem)
		if err != nil {
			panic(err)
		}
		b = append(b, c)
	}

	fmt.Println(b)
	var min int = b[0]
	var max int = b[0]

	for _, elem := range b {
		if min > elem {
			min = elem
		}
		if elem > max {
			max = elem
		}
	}
	return strconv.Itoa(max) + " " + strconv.Itoa(min)
}
```
